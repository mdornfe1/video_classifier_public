#!/usr/bin/python3

"""
Classes for quick creation of Qt GUIs.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import sys 
import matplotlib as mpl
mpl.use("Qt5Agg")
import matplotlib.pyplot as plt, numpy as np
from matplotlib.backends.backend_qt5agg import (FigureCanvasQTAgg, 
	NavigationToolbar2QT as NavigationToolbar)
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from functools import partial
from collections import Callable
from IPython import embed

LEFT_KEY = 16777234
UP_KEY = 16777235
RIGHT_KEY = 16777236
DOWN_KEY = 16777237

app = QtWidgets.QApplication(sys.argv)

class Button(QtWidgets.QPushButton):
	"""
	QPushButton extended for use in QGridLayout.
	"""
	def __init__(self, row, col, updater, name):
		"""
		row : int
			Row in QGridLayout

		col : int
			Column in QGridLayout

		updater : Callable
			Function called when button is pressed.

		name : str
			Name of button

		"""
		super().__init__(name)
		self.row = row
		self.col = col
		self.updater = updater
		self.name = name
		self.clicked.connect(updater)

class TextBox:
	"""
	TextBox for use in QGridLayout. This class is composed with a QLineEdit
	object and a QLabel object so a labeled QLineEdit can be placed in
	QGridLayout.
	"""
	def __init__(self, row, col, value, name, 
		data_type, updater, parent_window):
		"""
		row : int
			Row in QGridLayout

		col : int
			Column in QGridLayout

		value : str
			Initial value of QLineEdit.

		name : str
			Name of TextBox. Also used as the text for self.label.

		data_type : type
			Data type of QLineEdit value.

		updater : Callable
			Function that is called when the value of self.line_edit is 
			changed. The default value of updater is None. If left to the
			default self._default_update_fun will be used.

		parent_window : DataWindow 
			Parent window of textbox.
		"""

		self.row = row
		self.col = col
		self.updater = updater
		self.value = value
		self.name = name
		self.data_type = data_type
		self.parent_window = parent_window

		self.line_edit = self.add_line_edit(value, updater)
		self.label = self.add_label_text(name)


	def add_line_edit(self, value, updater=None):
		"""
		value : str
			Default value of QLineEdit

		updater : callable
			Function to be called when the value of QLineEdit is changed

		Returns

		line_edit : QLineEdit
			QLineEdit object, connected to function updater, with default 
			text set to value.

		"""
		line_edit = specialKeyEventQLineEdit()
		line_edit.setText( str(value) )
		if updater is None:
			updater = partial(self._default_update_fun, self.name)
			line_edit.textChanged[str].connect(updater)
		else:
			line_edit.textChanged[str].connect(updater)

		return line_edit

	def set_value(self, value):
		"""
		value :
			New value of self.line_edit.
		"""
		self.line_edit.setText( str(value) )

	def add_label_text(self, name):
		"""
		name : str
			Name of QLabel

		Returns

		label : QLabel
			QLabel with text set to name.
		"""
		label = QtWidgets.QLabel()
		label.setText(name)

		return label

	def _default_update_fun(self, name, text):
		"""
		name : str
			Name of TextBox to update.

		text : str
			New text value of TextBox.
		"""
		if self.data_type == float:
			self.parent_window.dock.values[name] = float(text)
			try:
				value = int(text)
				self.parent_window.dock.values[name] = value
			except:
				self.set_value(0.0)
				self.parent_window.dock.values[name] = 0.0
		elif self.data_type == int:
			try:
				value = int(text)
				self.parent_window.dock.values[name] = value
			except:
				self.set_value(0)
				self.parent_window.dock.values[name] = 0

		elif self.data_type == str:
			self.parent_window.dock.values[name] = text
		else:
			raise TypeError('Text box must contain float, str, or int.')


class specialKeyEventQLineEdit(QtWidgets.QLineEdit):
	def keyPressEvent(self, event):
		if event.key()==LEFT_KEY or event.key()==RIGHT_KEY: 
			self.parent().keyPressEvent(event)
		else:
			super().keyPressEvent(event)

class noKeyEventQListWidget(QtWidgets.QListWidget):
	def keyPressEvent(self, event):
		pass
		self.parent().keyPressEvent(event)

class Dock(QtWidgets.QDockWidget):
	def __init__(self, parent_window):
		super().__init__()
		self.controls = QtWidgets.QWidget()
		self.setWidget( self.controls)
		self.controls_grid = QtWidgets.QGridLayout(self.controls)

		self.buttons = {}
		self.text_boxes = {}
		self.check_boxes = {}
		self.texts = {}
		self.values = {}
		self.toolbar_stack = None
		self.page_list = noKeyEventQListWidget()

		self.parent_window = parent_window


	def add_widget(self, row, col, widget):
		self.controls_grid.addWidget(widget, row, col)


	def add_button(self, row, col, updater, name):
		button = Button(row, col, updater, name)
		self.buttons[name] = button
		self.controls_grid.addWidget(button, button.row, button.col)

	def add_textbox(self, row, col, name, data_type, value, updater=None):
		self.values[name] = value

		if updater is None:
			text_box = TextBox(row, col, value, name, data_type, updater, self.parent_window)
		else:
			updater = partial(updater, self.parent_window, name)
			text_box = TextBox(row, col, value, name, data_type, updater, self.parent_window)

		self.text_boxes[name] = text_box
		self.controls_grid.addWidget(text_box.line_edit, text_box.row, 
			text_box.col)
		self.controls_grid.addWidget(text_box.label, text_box.row, 
			text_box.col+1)

	def add_text(self, row, col, label, value):
		text = QtWidgets.QLabel()
		text.setText(value)
		self.texts[label] = text
		self.controls_grid.addWidget(text, row, col)

	def add_checkbox(self, row, col, label, updater, checked):
		check_box = QtWidgets.QCheckBox(label)
		check_box.setChecked(checked)
		check_box.stateChanged.connect(updater)
		self.controls_grid.addWidget(check_box, row, col)
		self.check_boxes[label] = check_box
		
		
	def edit_text_box(self, label, value):
		self.values[label] = value
		self.text_boxes[label].set_value(value)

	def edit_text(self, label, value):
		self.texts[label].setText(value)


	def add_toolbar(self, row, col, canvas):
		if self.toolbar_stack is None:
			self.toolbar_stack = QtWidgets.QStackedWidget(self)
			self.controls_grid.addWidget( self.toolbar_stack, row, col)

		self.toolbar = NavigationToolbar(canvas, self)
		self.toolbar_stack.addWidget(self.toolbar)

	def add_page_list(self, row, col):
		self.controls_grid.addWidget(self.page_list, row, col)


class DataWindow(QtWidgets.QMainWindow):
	def __init__(self):

		super().__init__()

		self.figs = []
		self.axs_list = []
		self.updaters = []
		self.key_event_functions = {}
		self.current_page = 0

		self.central_widget = QtWidgets.QStackedWidget(self)
		self.setCentralWidget(self.central_widget)

		self.add_dock()


	def closeEvent(self, event):
		plt.close('all')
		event.accept()


	def keyPressEvent(self, event):
		if event.key() in self.key_event_functions:
			self.key_event_functions[event.key()].__call__()


	def add_key_event(self, key_id, event_function):
		self.key_event_functions[key_id] = event_function


	def add_page(self, page_name:str, rows:int, cols:int, updater:Callable):
		
		page_number = len(self.figs)
		self.dock.page_list.insertItem(page_number, page_name )
		self.dock.page_list.currentRowChanged.connect(self._change_page)

		fig, axs  = plt.subplots(rows, cols)

		canvas = FigureCanvasQTAgg(fig)
		self.dock.add_toolbar(0, 0, canvas)
		self.central_widget.addWidget( canvas )

		self.figs.append(fig)
		self.axs_list.extend([axs])
		self.updaters.append(updater)


	def add_dock(self):
		self.dock = Dock(self)
		self.addDockWidget(Qt.BottomDockWidgetArea, self.dock)		

		return self.dock


	def _change_page(self, page):

		self.current_page = page
		self.central_widget.setCurrentIndex(page)
		self.dock.toolbar_stack.setCurrentIndex(page)

	def _update_figs(self):

		([fig_update(axs, self.dock.values) 
			for fig_update, axs in zip(self.updaters, self.axs_list)])

		[fig.canvas.draw() for fig in self.figs]


	def show(self):
		#self._update_figs()
		super().show()
		sys.exit(app.exec_())