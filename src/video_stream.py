#!/usr/bin/python3

"""
Class for Pythonic indexing and interation of a video file.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import imageio
from skimage import color
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os
from secrets import local_data_directory

plt.style.use('bmh')

class VideoStream:
	"""
	Interface to provide easy iteration and indexing access to imageio
	video objects: 
	self[10] returns the 10^th frame of the video.
	self[10:20] returns frames 10-20 as a list
	"""
	def __init__(self, file, color=True):
		"""
		file : str
			uri of video file
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images. Defaults to True.
		"""
		self.video = imageio.get_reader(file,  'ffmpeg')
		self.color = color
		
		self.duration = self.video.get_meta_data()['duration']
		self.fps = self.video.get_meta_data()['fps']
		self.nframes = self.video.get_meta_data()['nframes']
		self.resolution = self.video.get_meta_data()['size']

		self.frame_number = 0

	def __len__(self):
		"""
		Returns number of frames in video.
		"""
		return self.nframes

	def __getitem__(self, frame_number):
		"""
		frame_number : int or slice
			frame number or slice of frame numbers

		Returns
			frame : np.ndarray or List[np.ndarray]
				If frame_number is int __getitem__ returns a single numpy array which 
				represents the pixel values of the image. If frame_number is a slice
				of frame numbers __getitem__ returns a list of numpy arrays
				which represent the pixel values of the images.

		"""
		if isinstance(frame_number, slice):
			indices = range(*frame_number.indices(len(self)))
			return [self._get_frame(i) for i in indices]
		else:
			return self._get_frame(frame_number)

	def __next__(self):
		"""
		Method for iterating over frames in video object. Returns next frame
		in video.
		"""
		if self.frame_number > self.nframes:
			self.frame_number = 0
			raise StopIteration 
		else:
			self.frame_number += 1
			return self.__getitem__(self.frame_number-1)

	def __iter__(self):
		return self

	def _get_frame(self, frame_number):
		"""
		frame_number : int
			Integer which indexes the frame in the video,

		Returns
			frame : np.ndarray
				A numpy array which stores the pixel values of the image.
				If self.color is True frame.shape = (*image.shape, 3).
				If self.color is False frame.shape = (*image.shape, 1).
		"""

		frame = self.video.get_data(frame_number)
		if self.color:
			return frame
		else:
			return color.rgb2gray(frame)

	def set_color(self, color):
		"""
		color : bool
			Pass true for VideoStream to return color images. Pass false
			for VideoStream to return b+w images.
		"""
		self.color = color

	def plot_frame(self, frame_number):
		"""
		frame_number : int
			Pass frame_number of frame to be plotted.
		Returns 
			fig, ax, im of plotted frame

		Plots frame self[frame_number]
		"""
		fig, ax = plt.subplots()
		frame = self[frame_number]

		if self.color:
			im = ax.imshow(frame)
		else:
			im = plt.imshow(frame, cmap='Greys')

		return fig, ax, im

if __name__ == '__main__':
	video_files = os.listdir(data_directory)
	video_file = '{}/{}'.format(local_data_directory, video_files[1])
	vs = VideoStream(video_file, color=False)
	vs.set_color(True)
	fig, ax, im = vs.plot_frame(2347)
	plt.show()

