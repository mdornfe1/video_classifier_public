#!/usr/bin/python3

"""
Script for starting video_classifer program.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import os
from mount import mount_sshfs
from add_videos import populate_video_data_collection
from mongo_interface import MongoInterface
from frame_viewer import FrameViewer
from secrets import (server_name, host_username, mongo_username, 
	mongo_password, host_ip, host_data_directory, local_data_directory, 
	key_file)
import socket


if __name__ == '__main__':
	if server_name != socket.gethostname():
		mount_sshfs(local_data_directory, host_data_directory, host_username, 
			host_ip, key_file)
		
	mi = MongoInterface(mongo_username, mongo_password, host_ip, 
		db='dolphin_images')
	populate_video_data_collection(mi, local_data_directory)
	frame_viewer = FrameViewer()
	frame_viewer.show()


