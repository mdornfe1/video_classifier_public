#!/usr/bin/python3

"""
Script for adding new videos to the database.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import os
import re
from dateutil import parser
from datetime import timedelta
import imageio
from secrets import mongo_username, mongo_password, host_ip, local_data_directory
from mongo_interface import MongoInterface

CAMERAS = ['left', 'right', 'main']

def get_avi_files(video_folder:str):
	video_files = []
	for filename in os.listdir(video_folder):
		name, ext = os.path.splitext(filename)
		if ext == '.avi':
			video_files.append(filename)

	return video_files

def get_mp4_files(video_folder:str):
	video_files = []
	for filename in os.listdir(video_folder):
		name, ext = os.path.splitext(filename)
		if ext == '.mp4':
			video_files.append(filename)

	return video_files


def extract_start_date(filename:str):
	return re.search('\d{4}-\d{2}-\d{2}', filename).group()

def extract_end_date(filename:str):
	filename = filename[::-1]
	return re.search('\d{2}-\d{2}-\d{4}', filename).group()[::-1]

def extract_start_time(filename:str):
	start_time = re.search('\d{2}_\d{2}_\d{2}', filename).group()
	return start_time.replace('_', ':')

def extract_end_time(filename:str):
	filename = filename[::-1]
	start_time = re.search('\d{2}_\d{2}_\d{2}', filename).group()
	return start_time.replace('_', ':')[::-1]

def extract_camera_name(filename:str):
	return list(filter(lambda camera: camera in filename, CAMERAS))[0]

def populate_video_data_collection(mi:MongoInterface, video_folder:str):
	mp4_files = get_mp4_files(video_folder)
	avi_files = get_avi_files(video_folder)
	video_files = mp4_files + avi_files
	mi.set_collection('video_data')
	for filename in video_files:
		start_date = extract_start_date(filename)
		start_time = extract_start_time(filename)
		end_date = extract_end_date(filename)
		end_time = extract_end_time(filename)
		camera_name = extract_camera_name(filename)
		
		start_dt = parser.parse('{} {}'.format(start_date, start_time), ignoretz=True)
		end_dt = parser.parse('{} {}'.format(end_date, end_time), ignoretz=True)

		full_path = os.path.join(video_folder, filename)

		while True:
			try:
				video = imageio.get_reader(full_path,  'ffmpeg')		
			except:
				continue

			break
		
		meta_data = video.get_meta_data()
		fps = meta_data['fps']
		nframes = meta_data['nframes']
		resolution = meta_data['size']

		video_data = {'_id': filename, 'start_dt':start_dt,  
		'end_dt':end_dt, 'fps':fps, 'nframes':nframes, 
		'resolution':resolution, 'current_frame_number': 0,
		'frame_skip': 100, 'n_dolphins': 0}

		if filename not in mi:
			mi.insert_document(video_data)
			

if __name__ == '__main__':
	#host_ip = 'localhost'
	video_db = MongoInterface(mongo_username, mongo_password, 
			host_ip, db='dolphin_images', collection='video_data')
	populate_video_data_collection(video_db, local_data_directory)