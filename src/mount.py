#!/usr/bin/python3

"""
All videos are stored in host_data_directory on host_ip. This script uses
sshfs to mount and unmount host_data_directory on local host. The script
frame_viewer.py then makes a connection to the videos mounted in 
local_data_directory.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import subprocess 
import platform
from secrets import (host_username, host_ip, host_data_directory, key_file, 
	local_data_directory)


def unmount(local_data_directory):
	"""
	local_data_directory : str
		Directory in which host_data_directory is locally mounted.

	Unmount host_data_directory from local host.
	"""
	os = platform.platform()
	if 'Darwin' in os:
		unmount_command = 'diskutil unmount {}'.format(local_data_directory)
	else:
		unmount_command = 'fusermount -u {}'.format(local_data_directory)
	subprocess.call(unmount_command, shell=True)

def mount_sshfs(local_data_directory:str, host_data_directory:str, 
	host_username:str, host_ip:str, key_file:str):
	"""
	local_data_directory : str
		Directory in which host_data_directory is locally mounted.

	host_data_directory : str
		Directory on remote machine which stores video files.

	host_usename : str
		Username to login to machine at host_ip.

	host_ip : str
		IP address of host machine.

	key_file : str
		SSH key file used to login to host machine.

	Locally mounts host_data_directory in local_data_directory. For this to 
	work you need the correct host_username, host_ip, and key_file stored in 
	secrets.py. You also also need a valid ssh key file stored in location
	key_file.
	"""

	unmount(local_data_directory)
	mount_command = 'sshfs -o ro -o allow_other -o IdentityFile={} {}@{}:{} {}'.format(
		key_file, host_username, host_ip, host_data_directory, local_data_directory)
	subprocess.call(mount_command, shell=True)

if __name__ == '__main__':
	mount_sshfs(local_data_directory, host_data_directory, host_username, 
		host_ip, key_file)