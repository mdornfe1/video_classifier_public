#!/usr/bin/python3

"""
Class for Pythonic interface to a MongoDB collection.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import pymongo
from secrets import mongo_username, mongo_password, host_ip


class MongoInterface:
	"""
	Pythonic interface to a MongoDB collection.
	"""
	def __init__(self, username='', password='', ip='localhost', port='27017', db='', collection=''):
		"""
		username : str
			MongoDB username.

		password : str
			MongoDB password.

		ip : str
			MongoDB ip address.

		db : str
			MongoDB database name.

		collection : str
			MongoDB collection name
		"""

		uri = 'mongodb://{}:{}@{}:{}/{}'.format(username, password, ip, port, db)
		self.client = pymongo.MongoClient(uri)

		if len(db) != 0:
			self.set_db(db)

		if len(collection) != 0:
			self.set_collection(collection)
			self.keys = self.get_keys()

		self.index = 0


	def __next__(self):
		"""
		Method for iterating over keys in MongoDB collection. This is 
		merely a convenience function. The order of the keys has no meaning.
		"""
		if self.index >= len(self):
			self.index = 0
			raise StopIteration 
		else:
			self.index += 1
			return self[self.index-1]


	def __iter__(self):
		return self


	def __getitem__(self, index):
		"""
		index : int or slice
			Index or indices of document(s) in MongoDB collection.

		Returns

		document : dict or List[dict]
			If index is int this method returns a MongoDB document in dict 
			form. If index is a slice this method returns a list of MongoDB
			documents in dict form. 

		"""
		if self.collection is None:
			raise ValueError(
				"""The attribute self.collection must be set before 
				method self.__getitem__ can be used.""")
		
		if isinstance(index, slice):
			indices = index.indices(len(self))
			keys = self.keys[index]
			return [self.get_document(key) for key in keys]
		else:
			key = self.keys[index] 
			return self.get_document(key)


	def __len__(self):
		"""
		Returns number of documents in collection.
		"""
		return len(self.keys)

	def __contains__(self, key):
		"""
		key : Hashable
			Key of MongoDB document.

		Returns True if key in collection False if not.
		"""
		return True if key in self.keys else False


	def set_db(self, db):
		"""
		db : str
			Name of new database.
			
		Change MongoDB database.
		"""
		self.db = self.client.get_database(db)


	def set_collection(self, collection):
		"""
		collection : str
			Name of new collection

		Change MongoDB collection
		"""


		if self.db is None:
			raise ValueError(
				"The attribute db must be set before the attribute collection can be set.")

		self.collection = self.db.get_collection(collection)
		self.keys = self.get_keys()


	def insert_document(self, document):
		"""
		document : dict
			New MongoDB document

		Insert document into collection.
		"""

		self.collection.insert_one(document)
		self.keys = self.get_keys()


	def delete_document(self, key):
		"""
		key : Hashable
			Key of MongoDB document.

		Delete document associated with key. Be careful. 
		"""
		self.collection.delete_one({'_id': key })
		self.keys = self.get_keys()


	def get_document(self, query):
		"""
		query : dict
			MongoDB query in the form of a dict.

		Returns 

		document : dict
			Document thats the product of the query.
		"""
		document = self.collection.find_one(query)

		return document

	def find(self, query):
		"""
		query : dict
			MongoDB query in the form of a dict.

		Returns 

		documents : pymongo.cursor
			Cursor that can be used to iterate over documents that satisfy
			the query

		Method for querying multiple documents from collection.
		"""
		return self.collection.find(query)

	def update_document(self, key, document):
		"""
		key : Hashable
			Key of existing document in collection.

		document : dict
			New document for which key will now reference.

		Updates documents associated with existing key.
		"""
		self.collection.update({'_id':key}, {"$set": document}, upsert=False)


	def get_keys(self):
		"""
		Returns all keys in collection.
		"""
		return self.collection.distinct('_id')


	def close(self):
		"""
		Closes connection to database.
		"""
		self.client.close()



if __name__ == '__main__':
	#host_ip = 'localhost'
	mif = MongoInterface(mongo_username, mongo_password, 
			host_ip, db='dolphin_images', collection='frame_data')
	miv = MongoInterface(mongo_username, mongo_password, 
			host_ip, db='dolphin_images', collection='video_data')
"""
for document in miv:
	_id = document['_id']
	miv.delete_document(_id)
"""