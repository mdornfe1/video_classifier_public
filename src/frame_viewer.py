#!/usr/bin/python3

"""
GUI for browsing through still frames in video and store information about
the frames in database.

Author: Matthew Dornfeld
email: mdornfe1@gmail.com
"""

import sys, os, getpass, datetime
import numpy as np
import pymongo
from PyQt5.QtCore import Qt

from video_stream import VideoStream
import gui_maker as gm
from mongo_interface import MongoInterface
import mount
from secrets import (mongo_username, mongo_password, host_ip, 
	local_data_directory)


LEFT_KEY = 16777234
UP_KEY = 16777235
RIGHT_KEY = 16777236
DOWN_KEY = 16777237

#HDF5_FILE = os.path.join(local_data_directory, 'dolphin_videos.hdf5')

def frame_updater(ax, values):
	frame = values['current_frame']
	ax.imshow(frame)

class FrameViewer(gm.DataWindow):
	"""
	Class to view and classify frames from video files with the number 
	of dolphins in the frame.
	"""
	def __init__(self):
		super().__init__()
		self.user = getpass.getuser()
		self._setup_mongodb_interfaces()
		self._setup_ui()


	def closeEvent(self, event):
		self._update_video_db()
		super().closeEvent(event)
		self.video_stream.video.close()
		mount.unmount(local_data_directory)


	def up_key_event(self):
		"""
		Pressing the up key increase the number of dolphins in the frame tag 
		by 1.
		"""
		self.dock.values['n_dolphins'] += 1
		self.dock.edit_text_box('n_dolphins', self.dock.values['n_dolphins'])


	def down_key_event(self):
		"""
		Pressing the down key decrease the number of dolphins in the frame tag
		by 1.
		"""
		self.dock.values['n_dolphins'] -= 1
		self.dock.values['n_dolphins'] = max(0, self.dock.values['n_dolphins'])
		self.dock.edit_text_box('n_dolphins', self.dock.values['n_dolphins'])


	def _setup_mongodb_interfaces(self):
		"""
		Data about the videos is stored in collection video_data, and data 
		about the frames for a video is stored in colletion frame_data.
		Calling this method setups MongoInterface connections to those 
		collections.
		"""
		self.video_db = MongoInterface(mongo_username, mongo_password, 
			host_ip, db='dolphin_images', collection='video_data')
		self.frame_db = MongoInterface(mongo_username, mongo_password, 
			host_ip, db='dolphin_images', collection='frame_data')


	def _setup_ui(self):
		"""
		Setup the GUI here.
		"""
		self.add_page(page_name='frame_viewer', rows=1, cols=1, 
			updater=frame_updater)
		
		#add list of video files in project
		self.file_list = gm.noKeyEventQListWidget()
		self.file_list.setFixedSize(400, 100)
		row, col = (1, 0)
		self.dock.controls_grid.addWidget(self.file_list, row, col)
		self.file_list.currentRowChanged.connect(self.change_video)

		for n, filename in enumerate(os.listdir(local_data_directory)):
			self.file_list.insertItem(n, filename)

		self.dock.add_button(row=1, col=2, updater=self.previous_frame, 
			name='Previous Frame')
		self.dock.add_button(row=1, col=3, updater=self.next_frame, 
			name='Next Frame')
		self.dock.add_button(row=2, col=2, updater=self.goto_first_frame, 
			name='First Frame')
		self.dock.add_button(row=2, col=3, updater=self.goto_last_frame, 
			name='Last Frame')
		
		self.dock.add_textbox(row=2, col=0, name='frame_skip', 
			data_type=int, value=100)
		self.dock.add_textbox(row=5, col=0, name='n_dolphins', 
			data_type=int, value=0)

		self.dock.add_text(row=6, col=0, label='frame_number', 
			value='frame=')
		self.dock.add_text(row=7, col=0, label='time', 
			value='time=')
		self.dock.add_text(row=8, col=0, label='n_dolphins', 
			value='n_dolphins=')

		self.dock.add_checkbox(row=9, col=0, label='review mode', 
			updater=self.change_review_mode, checked=False)

		self.add_key_event(UP_KEY, self.up_key_event)
		self.add_key_event(DOWN_KEY, self.down_key_event)
		self.add_key_event(LEFT_KEY, self.previous_frame)
		self.add_key_event(RIGHT_KEY, self.next_frame)


	def _update_frame_db(self):
		"""
		Sends data about the video frame in the current GUI view to 
		frame_data collection.
		"""
		filename = self.video_data['_id']
		frame_number = self.video_data['current_frame_number']
		n_dolphins = self.dock.values['n_dolphins']
		_id = '{}?frame_number={}'.format(filename, frame_number)
		current_dt = datetime.datetime.now()

		frame_data = {'_id': _id, 'filename': filename, 
		'frame_number': frame_number, 'n_dolphins': n_dolphins, 
		'user':self.user, 'last_updated': current_dt}

		if _id in self.frame_db:
			self.frame_db.update_document(_id, frame_data)
		else:
			self.frame_db.insert_document(frame_data)


	def _update_video_db(self):
		"""
		Sends data about the the current video to video_data collection.
		"""
		filename = self.video_data['_id']
		self.video_data['frame_skip'] = self.dock.values['frame_skip']
		self.video_data['n_dolphins'] = self.dock.values['n_dolphins']
		self.video_db.update_document(filename, self.video_data)


	def _change_frame(self, current_frame_number):
		"""
		current_frame_number : int
			The index of the frame in the current GUI view.

		Private method to be called whenever the displayed frame is changed.
		Updates the databases and GUI display information.
		"""
		if (current_frame_number < 0 
			or current_frame_number >= len(self.video_stream)):
			return

		self._update_frame_db()

		current_frame = self.video_stream[current_frame_number]

		display_text = 'frame = {}/{}'.format(
			current_frame_number, len(self.video_stream))
		self.dock.edit_text('frame_number', display_text)

		fps = self.video_data['fps']
		minutes, seconds = divmod(current_frame_number / float(fps), 60)
		hours, minutes = divmod(minutes, 60)
		
		display_text = 'time = {}:{}:{}'.format(
			int(hours), int(minutes), round(seconds,2))
		self.dock.edit_text('time', display_text)

		filename = self.video_data['_id']
		frame_db_key = '{}?frame_number={}'.format(
			filename, current_frame_number)

		if frame_db_key in self.frame_db:
			frame_data = self.frame_db.get_document(frame_db_key)
			n_dolphins = frame_data['n_dolphins']

			display_text = 'n_dolphins = {}'.format(n_dolphins)
		else:
			display_text = 'n_dolphins = '
		
		self.dock.edit_text('n_dolphins', display_text)


		self.video_data['current_frame_number'] = current_frame_number
		self.dock.values['current_frame'] = current_frame


	def change_review_mode(self, current_state):
		"""
		current_state : Qt.CheckState
			True if app is review mode. False if not.

		Toggles frame_viewer to review mode. When in review mode 
		next_frame and previous_frame will only show previously tagged data.
		"""

		self.dock.buttons['Next Frame'].clicked.disconnect()
		self.dock.buttons['Previous Frame'].clicked.disconnect()
		
		if current_state == Qt.Checked:
			self.dock.buttons['Next Frame'].clicked.connect(
				self.review_next_frame) 
			self.dock.buttons['Previous Frame'].clicked.connect(
				self.review_previous_frame)
			self.key_event_functions[LEFT_KEY] = self.review_previous_frame
			self.key_event_functions[RIGHT_KEY] = self.review_next_frame
		else:
			self.dock.buttons['Next Frame'].clicked.connect(
				self.next_frame) 
			self.dock.buttons['Previous Frame'].clicked.connect(
				self.previous_frame)  
			self.key_event_functions[LEFT_KEY] = self.previous_frame
			self.key_event_functions[RIGHT_KEY] = self.next_frame



	def change_video(self, file_number):
		"""
		file_number : int
			Number of file in self.file_list

		Changes current video to that of file_number in file_list. Sends
		self.video_data back to MongoDB dolphin_images before changing
		videos
		"""

		if hasattr(self, 'video_data'):
			self._update_video_db()


		filename = self.file_list.item(file_number).text()
		self.video_data = self.video_db.get_document(filename)
		full_path = os.path.join(local_data_directory, filename)
		self.video_stream = VideoStream(full_path)
		
		current_frame_number = self.video_data['current_frame_number']
		frame_skip = self.video_data['frame_skip']
		n_dolphins = self.video_data['n_dolphins']
		self.dock.edit_text_box('frame_skip', frame_skip)
		self.dock.edit_text_box('n_dolphins', n_dolphins)
		self.dock.values['current_frame'] = self.video_stream[current_frame_number]

		self._change_frame(current_frame_number)
		self._update_figs()


	def next_frame(self):
		"""
		Callback function for button 'next_frame'. Send data about current 
		frame to MongoDB then loads and displays the next frame in the video.
		"""
		frame_skip = self.dock.values['frame_skip']
		current_frame_number = self.video_data['current_frame_number']

		next_frame_number = current_frame_number + frame_skip

		self._change_frame(next_frame_number)
		self._update_figs()


	def previous_frame(self):
		"""
		Callback function for button 'previous_frame'. Send data about current 
		frame to MongoDB then loads and displays the previous frame in the 
		video.
		"""
		frame_skip = self.dock.values['frame_skip']
		current_frame_number = self.video_data['current_frame_number']
		
		previous_frame_number = current_frame_number - frame_skip

		self._change_frame(previous_frame_number)
		self._update_figs()


	def review_previous_frame(self):
		"""
		Callback function for button 'previous_frame' when in review mode. 
		Loads and displays the previous frame, which has already been tagged,
		and does not update the database.
		"""
		current_frame_number = self.video_data['current_frame_number']
		filename = self.video_data['_id']

		query = {'frame_number': {'$lt': current_frame_number}, 
		'filename': filename}
		result = self.frame_db.find(query).sort('frame_number', 
			pymongo.DESCENDING)
	
		if result.count() == 0:
			return
		else:
			previous_frame_data = result[0]

		previous_frame_number = previous_frame_data['frame_number']

		self._change_frame(previous_frame_number)
		self._update_figs()

	def review_next_frame(self):
		"""
		Callback function for button 'next_frame' when in review mode. 
		Loads and displays the next frame, which has already been tagged,
		and does not update the database.
		"""
		current_frame_number = self.video_data['current_frame_number']
		filename = self.video_data['_id']

		query = {'frame_number': {'$gt': current_frame_number}, 
		'filename': filename}
		result = self.frame_db.find(query).sort('frame_number', 
			pymongo.ASCENDING)

		if result.count() == 0:
			return
		else:
			next_frame_data = result[0]

		next_frame_number = next_frame_data['frame_number']

		self._change_frame(next_frame_number)
		self._update_figs()

	def goto_last_frame(self):
		"""
		Goto last tagged frame in the current video
		"""
		filename = self.video_data['_id']

		query = {'filename': filename}
		result = self.frame_db.find(query).sort('frame_number', 
			pymongo.DESCENDING)

		if result.count() == 0:
			return
		else:
			next_frame_data = result[0]

		next_frame_number = next_frame_data['frame_number']

		self._change_frame(next_frame_number)
		self._update_figs()

	def goto_first_frame(self):
		"""
		Goto first tagged frame in the current video
		"""
		filename = self.video_data['_id']

		query = {'filename': filename}
		result = self.frame_db.find(query).sort('frame_number', 
			pymongo.ASCENDING)

		if result.count() == 0:
			return
		else:
			next_frame_data = result[0]

		next_frame_number = next_frame_data['frame_number']

		self._change_frame(next_frame_number)
		self._update_figs()


if __name__ == '__main__':
	frame_viewer = FrameViewer()
	frame_viewer.show()
